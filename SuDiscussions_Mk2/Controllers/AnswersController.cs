﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuDiscussionsDAO;

namespace SuDiscussions_Mk2.Controllers
{
    public class AnswersController : Controller
    {
        private SuDiscussDBEntities db = new SuDiscussDBEntities();

        public ActionResult PostAnswer(int? qid)
        {
            if (qid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session["qid"] = qid;
            Question question = db.Questions.Find(qid);
            Answer answer = new Answer();
            answer.questionid = (int)qid;
            answer.Question = question;
            return View(answer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostAnswer([Bind(Include = "id,description,userid,questionid")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                answer.questionid = (int)Session["qid"];
                answer.userid = (int)Session["id"];
                db.Answers.Add(answer);
                db.SaveChanges();
                return RedirectToAction("ViewQuestion", "Questions", new { id = answer.questionid });
            }
            return View(answer);
        }

        public ActionResult EditPost(int? id)
        {
            try
            {
                if ((int)Session["logged_in"] == 0)
                {
                    return RedirectToAction("Login", "Users");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Answer answer = db.Answers.Find(id);
                if (answer == null)
                {
                    return HttpNotFound();
                }
                if ((int)Session["id"] != answer.userid)
                {
                    return RedirectToAction("Logout", "Users");
                }
                return View(answer);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Users"); }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost([Bind(Include = "id,description,userid,questionid")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ViewQuestion","Questions",new { id = answer.questionid });
            }
            return View(answer);
        }

        public ActionResult DeletePost(int? id)
        {
            try
            {
                if ((int)Session["logged_in"] == 0)
                {
                    return RedirectToAction("Login", "Users");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Answer answer = db.Answers.Find(id);
                if (answer == null)
                {
                    return HttpNotFound();
                }
                if ((int)Session["id"] != answer.userid)
                {
                    return RedirectToAction("Logout", "Users");
                }
                return View(answer);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Users"); }
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("DeletePost")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePostConfirmed(int id)
        {
            Answer answer = db.Answers.Find(id);
            int qid = answer.questionid;
            db.Answers.Remove(answer);
            db.SaveChanges();
            return RedirectToAction("ViewQuestion","Questions",new { id = qid });
        }

        // GET: Answers
        public ActionResult Index()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                var answers = db.Answers.Include(a => a.Question).Include(a => a.User);
                return View(answers.ToList());
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // GET: Answers/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Answer answer = db.Answers.Find(id);
                if (answer == null)
                {
                    return HttpNotFound();
                }
                return View(answer);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // GET: Answers/Create
        public ActionResult Create()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                ViewBag.questionid = new SelectList(db.Questions, "id", "title");
                ViewBag.userid = new SelectList(db.Users, "id", "firstname");
                return View();
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Answers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,description,userid,questionid")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Answers.Add(answer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.questionid = new SelectList(db.Questions, "id", "title", answer.questionid);
            ViewBag.userid = new SelectList(db.Users, "id", "firstname", answer.userid);
            return View(answer);
        }

        // GET: Answers/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Answer answer = db.Answers.Find(id);
                if (answer == null)
                {
                    return HttpNotFound();
                }
                ViewBag.questionid = new SelectList(db.Questions, "id", "title", answer.questionid);
                ViewBag.userid = new SelectList(db.Users, "id", "firstname", answer.userid);
                return View(answer);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Answers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,description,userid,questionid")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.questionid = new SelectList(db.Questions, "id", "title", answer.questionid);
            ViewBag.userid = new SelectList(db.Users, "id", "firstname", answer.userid);
            return View(answer);
        }

        // GET: Answers/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Answer answer = db.Answers.Find(id);
                if (answer == null)
                {
                    return HttpNotFound();
                }
                return View(answer);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Answers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Answer answer = db.Answers.Find(id);
            db.Answers.Remove(answer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
