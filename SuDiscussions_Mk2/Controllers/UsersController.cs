﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using SuDiscussionsDAO;

namespace SuDiscussions_Mk2.Controllers
{
    public class UsersController : Controller
    {
        private SuDiscussDBEntities db = new SuDiscussDBEntities();

        // GET: Users
        public ActionResult Index()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                return View(db.Users.ToList());
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        public ActionResult Login()
        {
            try
            {
                if ((int)Session["logged_in"] == 1)
                    return RedirectToAction("Logout");

                return View();
            }
            catch(NullReferenceException nosession) { return View(); }
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            var L1 = from usr in db.Users orderby usr.id select usr;

            if (ModelState.IsValid)
            {
                foreach (User userlist in L1)
                {
                    if (userlist.email == user.email && userlist.password == user.password)
                    {
                        Session["logged_in"] = 1;
                        Session["id"] = userlist.id;
                        Session["fname"] = userlist.firstname;
                        Session["lname"] = userlist.lastname; 
                        Session["email"] = userlist.email;

                        return RedirectToAction("Index", "Questions");
                    }

                }
                ModelState.AddModelError("password", "Username or Password Incorrect!");
            }
            return View(user);
        }

        public ActionResult Logout()
        {
            Session["logged_in"] = 0;
            Session["id"] = 0;
            Session["fname"] = null;
            Session["lname"] = null;
            Session["email"] = null;

            return RedirectToAction("Login", "Users");
        }
        /*
        static bool isValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            try
            {
                // Use MailAddress class to validate email format
                var addr = new MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
        */
        public ActionResult Register()
        {
            try
            {
                if ((int)Session["logged_in"] == 1)
                    return RedirectToAction("Logout");

                return View();
            }
            catch (NullReferenceException nosession) { return View(); }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "id,firstname,lastname,email,password")] User user)
        {
            if (ModelState.IsValid)
            {
                var isEmailAlreadyExists = db.Users.Any(x => x.email == user.email);
//                if (!isValidEmail(user.email))
//                    ModelState.AddModelError("email", "Invalid Email!");
                if (isEmailAlreadyExists)
                    ModelState.AddModelError("email", "Email already in use!");
                if (ModelState.IsValid)
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Login");
                }
            }
            return View(user);
        }

        public ActionResult UserProfile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                User user = db.Users.Find(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                return View(user);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                return View();
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,firstname,lastname,email,password")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                User user = db.Users.Find(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                return View(user);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,firstname,lastname,email,password")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                User user = db.Users.Find(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                return View(user);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            if(user.Questions != null)
            {
                foreach(Question question in user.Questions.ToList()) 
                {
                    if(question.Answers != null)
                    {
                        foreach (Answer answer in question.Answers.ToList())
                        {
                            db.Answers.Remove(answer);
                        }
                    }
                    db.Questions.Remove(question); 
                }
            }
            if(user.Answers != null) 
            {
                foreach(Answer answer in user.Answers.ToList())
                {
                    db.Answers.Remove(answer);
                }
            }
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
