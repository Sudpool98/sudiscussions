﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuDiscussionsDAO;

namespace SuDiscussions_Mk2.Controllers
{
    public class QuestionsController : Controller
    {
        private SuDiscussDBEntities db = new SuDiscussDBEntities();
        
        // GET: Questions
        public ActionResult Index(string search)
        {
            var questions = db.Questions.Include(q => q.User);
            if (!string.IsNullOrEmpty(search))
            {
                questions = questions.Where(q => q.title.Contains(search) || q.topic.Contains(search));
            }
            return View(questions.ToList());
        }

        public ActionResult ListAll()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                var questions = db.Questions.Include(q => q.User);
                return View(questions.ToList());
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        public ActionResult PostQuestion()
        {
            try
            {
                if ((int)Session["logged_in"] == 1)
                {
                    //ViewBag.userid = new SelectList(db.Users, "id", "firstname");
                    return View();
                }
                return RedirectToAction("Login", "Users");
            }
            catch(NullReferenceException nosession) { return RedirectToAction("Login", "Users"); }
        }

        // POST: Questions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostQuestion([Bind(Include = "id,title,description,userid,topic")] Question question)
        {
            if (ModelState.IsValid)
            {
                question.userid = (int)Session["id"];
                db.Questions.Add(question);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

 //           ViewBag.userid = new SelectList(db.Users, "id", "firstname", question.userid);
            return View(question);
        }

        public ActionResult ViewQuestion(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        public ActionResult EditPost(int? id)
        {
            try
            {
                if ((int)Session["logged_in"] == 0)
                {
                    return RedirectToAction("Login", "Users");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Question question = db.Questions.Find(id);
                if (question == null)
                {
                    return HttpNotFound();
                }
                if ((int)Session["id"] != question.userid)
                {
                    return RedirectToAction("Logout", "Users");
                }
                return View(question);
            }
            catch(NullReferenceException nosession) { return RedirectToAction("Login", "Users"); }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost([Bind(Include = "id,title,description,userid,topic")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ViewQuestion",new { id = question.id});
            }
            return View(question);
        }

        public ActionResult DeletePost(int? id)
        {
            try
            {
                if ((int)Session["logged_in"] == 0)
                {
                    return RedirectToAction("Login", "Users");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Question question = db.Questions.Find(id);
                if (question == null)
                {
                    return HttpNotFound();
                }
                if ((int)Session["id"] != question.userid)
                {
                    return RedirectToAction("Logout", "Users");
                }
                return View(question);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Users"); }
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("DeletePost")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePostConfirmed(int id)
        {
            Question question = db.Questions.Find(id);
            if(question.Answers != null)
            {
                foreach(Answer answer in question.Answers.ToList())
                {
                    db.Answers.Remove(answer);
                }   
            }
            db.Questions.Remove(question);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Questions/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Question question = db.Questions.Find(id);
                if (question == null)
                {
                    return HttpNotFound();
                }
                return View(question);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // GET: Questions/Create
        public ActionResult Create()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                ViewBag.userid = new SelectList(db.Users, "id", "firstname");
                return View();
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Questions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,title,description,userid,topic")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Questions.Add(question);
                db.SaveChanges();
                return RedirectToAction("ListAll");
            }

            ViewBag.userid = new SelectList(db.Users, "id", "firstname", question.userid);
            return View(question);
        }

        // GET: Questions/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Question question = db.Questions.Find(id);
                if (question == null)
                {
                    return HttpNotFound();
                }
                ViewBag.userid = new SelectList(db.Users, "id", "firstname", question.userid);
                return View(question);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Questions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,title,description,userid,topic")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListAll");
            }
            ViewBag.userid = new SelectList(db.Users, "id", "firstname", question.userid);
            return View(question);
        }

        // GET: Questions/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login", "Admins");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Question question = db.Questions.Find(id);
                if (question == null)
                {
                    return HttpNotFound();
                }
                return View(question);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login", "Admins"); }
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = db.Questions.Find(id);
            if (question.Answers != null)
            {
                foreach (Answer answer in question.Answers.ToList())
                {
                    db.Answers.Remove(answer);
                }
            }
            db.Questions.Remove(question);
            db.SaveChanges();
            return RedirectToAction("ListAll");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
