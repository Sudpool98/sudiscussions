﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuDiscussionsDAO;

namespace SuDiscussions_Mk2.Controllers
{
    public class AdminsController : Controller
    {
        private SuDiscussDBEntities db = new SuDiscussDBEntities();

        // GET: Admins
        public ActionResult Index()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login");
                }
                return View();
            }
            catch(NullReferenceException nosession) { return RedirectToAction("Login"); }
        }

        public ActionResult Login()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 1)
                    return RedirectToAction("Logout");

                return View();
            }
            catch(NullReferenceException nosesion) { return View();     }
        }

        [HttpPost]
        public ActionResult Login(Admin admin)
        {
            var L1 = from adm in db.Admins orderby adm.id select adm;

            if (ModelState.IsValid)
            {


                foreach (Admin adminlist in L1)
                {
                    if (adminlist.username == admin.username && adminlist.password == admin.password)
                    {
                        this.Session["loggedin_admin"] = 1;

                        return RedirectToAction("Index");
                    }

                }
                ModelState.AddModelError("incorrect", "Username or Password Incorrect!");
            }
            return View(admin);
        }

        public ActionResult Logout()
        {
            this.Session["loggedin_admin"] = 0;

            return RedirectToAction("Login");
        }

        // GET: Admins/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Admin admin = db.Admins.Find(id);
                if (admin == null)
                {
                    return HttpNotFound();
                }
                return View(admin);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login"); }
        }

        public ActionResult ListAll()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login");
                }
                return View(db.Admins.ToList());
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login"); }
        }

        // GET: Admins/Create
        public ActionResult Create()
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login");
                }
                return View();
            }
            catch(NullReferenceException nosession) { return RedirectToAction("Login"); }
        }

        // POST: Admins/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,username,email,password")] Admin admin)
        {
            if (ModelState.IsValid)
            {
                db.Admins.Add(admin);
                db.SaveChanges();
                return RedirectToAction("ListAll");
            }

            return View(admin);
        }

        // GET: Admins/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Admin admin = db.Admins.Find(id);
                if (admin == null)
                {
                    return HttpNotFound();
                }
                return View(admin);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login"); }
        }

        // POST: Admins/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,username,email,password")] Admin admin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListAll");
            }
            return View(admin);
        }

        // GET: Admins/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if ((int)Session["loggedin_admin"] == 0)
                {
                    return RedirectToAction("Login");
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Admin admin = db.Admins.Find(id);
                if (admin == null)
                {
                    return HttpNotFound();
                }
                return View(admin);
            }
            catch (NullReferenceException nosession) { return RedirectToAction("Login"); }
        }

        // POST: Admins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Admin admin = db.Admins.Find(id);
            db.Admins.Remove(admin);
            db.SaveChanges();
            return RedirectToAction("ListAll");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
